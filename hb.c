/*********************************************************************************************
* hb.c
* A simple Harris--Benedict calculator
* created by: Solt Budavari
*
* "The Harris-Benedict equation [...] is a method used
*  to estimate an individual's basal metabolic rate (BMR)
*  and daily calorie requirements."
*
* for men:   BMR = 66.4730 + 13.7516 * weight[kg] + 5.0033 * height[cm] - 6.7550 * age[years]
* for women: BMR = 655.0955 + 9.5634 * weight[kg] + 1.8496 * height[cm] - 4.6756 * age[years]
*
*  https://en.wikipedia.org/wiki/Harris-Benedict_equation
*
**********************************************************************************************/

/**********************************************************************
* Copyright (c) 2013 Solt Budavari
*
* hb is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

#define BF_M   66.4730    // "base factor" for males
#define BF_F  655.0955    // "base factor" for females
#define WF_M   13.7516    // "weight factors"
#define WF_F    9.5634
#define HF_M    5.0033    // "height factors"
#define HF_F    1.8496
#define AF_M    6.7550    // "age factors"
#define AF_F    4.6756

#include <stdio.h>
#include <stdlib.h>

int disp_help ( void )
{
    printf ( "Usage: hb [m|f] [weight in kgs] [height in cms] [age in years]\n" );
    return 0;
}

int main ( int argc, char* argv[] )
{
    int w, h, a;
    char* sex;

    if ( argc == 1 ) {
        disp_help () ;
        return 1;
    }
    if ( argv[1][0] != 'm' && argv[1][0] != 'f' ) {
        disp_help () ;
        return 1;
    }

    w = atoi ( argv[2] );
    h = atoi ( argv[3] );
    a = atoi ( argv[4] );
    sex = argv[1];

    printf ( "%.2f kcal/day\n", ( sex[0] == 'm' ) ? ( BF_M + ( WF_M * w ) + ( HF_M * h ) - ( AF_M * a ) ) : 
                                                    ( BF_F + ( WF_F * w ) + ( HF_F * h ) - ( AF_F * a ) ) );

    return 0;
}

